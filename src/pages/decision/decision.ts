import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Point } from '../../classes/point';

declare var Phaser;

@IonicPage()
@Component({
  selector: 'page-decision',
  templateUrl: 'decision.html',
})

export class DecisionPage {
  game: any;
  fakeData: any[];
  keyCounter: number;
  ball: any;
  sky: any;
  stage: any;
  scale: any;
  add: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    var thePoints = this.navParams.get('points')

    if(thePoints)
    console.log("Points from before " + JSON.stringify(thePoints))

    this.game = new Phaser.Game(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.AUTO, 'content', { preload: this.preload, create: this.create, updateBall: this.updateBall });
  }

  preload() {
    this.game.load.image('bg', 'assets/img/grass.jpg');
    this.game.load.image('ball', 'assets/img/cricketBall.png');

  }

  create() {
    this.game.physics.startSystem(Phaser.Physics.ARCADE);


    this.sky = this.add.image(0, 0, 'bg');
    this.sky.anchor.setTo(0.5, 0.5);
    this.sky.width = this.game.width*2;
    this.sky.height = this.game.height*2;

    this.stage.backgroundColor = '#131313';

    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    this.scale.maxWidth = this.game.width;
    this.scale.maxHeight = this.game.height;
    this.scale.pageAlignHorizontally = true;


    this.ball = this.game.add.sprite(100, 400, 'ball');
    this.game.physics.enable(this.ball, Phaser.Physics.ARCADE);
    this.ball.anchor.setTo(0.5, 0.5);
    this.ball.x = this.game.width - 10;
    this.ball.y = this.game.height - 10;
    this.ball.height = 100;
    this.ball.width = 100;



    this.fakeData = [{
        x: 125,
        y: 101,
        width: 200,
        height: 180,
        color: 'yellow'
      }, {
        x: 182,
        y: 120,
        width: 200,
        height: 180,
        color: 'yellow'
      }, {
        x: 173,
        y: 109,
        width: 200,
        height: 180,
        color: 'yellow'
      }];


      this.keyCounter = 0;

      setInterval(() => {
        this.updateBall();
      }, 1000);
  }

  updateBall() {
    if(this.keyCounter == 2){
      this.keyCounter = 0;
    }else{
        this.keyCounter++
    }

    if(this.keyCounter){
      if(this.ball.x != this.fakeData[this.keyCounter].x){
        this.ball.x = this.fakeData[this.keyCounter].x
        console.log("Balls x: " + this.ball.x)
      }

      if(this.ball.y != this.fakeData[this.keyCounter].y){
        this.ball.y = this.fakeData[this.keyCounter].y
        console.log("Balls y: " + this.ball.y)
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DecisionPage');

  }

}
