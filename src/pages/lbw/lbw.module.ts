import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LbwPage } from './lbw';

@NgModule({
  declarations: [
    LbwPage,
  ],
  imports: [
    IonicPageModule.forChild(LbwPage),
  ],
})
export class LbwPageModule {}
