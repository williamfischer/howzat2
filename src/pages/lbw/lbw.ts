import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CameraPreview, CameraPreviewOptions, CameraPreviewPictureOptions } from '@ionic-native/camera-preview';

import { DecisionPage } from '../decision/decision';
import {Observable} from 'rxjs/Rx';

import { Point } from '../../classes/point';


// import 'tracking';

import * as tracking from 'tracking/build/tracking';

@IonicPage()
@Component({
  selector: 'page-lbw',
  templateUrl: 'lbw.html',
})

export class LbwPage {

  tracker: any;
  picInfo: any;
  picture: any;
  yellowtrace: any[];
  loggedPoints: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private cameraPreview: CameraPreview) {
    this.loggedPoints = [];

    // console.log(this.loggedPoints)
    // var points = []
    // points.push(new Point(20,30,50,50,"yellow"))
    // console.log("Point at 0")
    // console.log(points[0].printPoint())
  }

  ionViewDidEnter(){

    const cameraPreviewOpts: CameraPreviewOptions = {
      x: 0,
      y: 0,
      width: window.screen.width,
      height: window.screen.height,
      camera: 'rear',
      tapPhoto: true,
      previewDrag: true,
      toBack: true,
      alpha: 1
    };

    this.cameraPreview.startCamera(cameraPreviewOpts).then((res) => {
      console.log(res);

        // if(this.loggedPoints.length >= 9){
        //   console.log("load next page")
        //   this.navCtrl.push(DecisionPage, {'points' : this.loggedPoints})
        // }
// =======
//       var photosTaken = 0;
//       while(photosTaken < 10){
//         this.takePicture().then((rect)=>{
//           if(rect){
//             this.loggedPoints.push(rect)
//           }
//           else{
//             console.log("No point found")
//           }
//           console.log("Logged points " + ' ' + JSON.stringify(this.loggedPoints))
//           photosTaken += 1;
//         })
//       }
// >>>>>>> defb7c9d9bf2f5c3eb5dff147545e018de2883ae
      var points = []
      var sub = Observable.interval(1500).subscribe((val) => {
        console.log('called');

        this.takePicture().then(res=>{
          console.log("Picture finished taking")
          var tracker = new tracking.ColorTracker(['yellow']);

          tracker.on('track', function(event) {
            if (event.data.length === 0) {
              // No colors were detected in this frame.
              console.log("Null")
            } else {
              event.data.forEach(function(rect) {
                // rect.x, rect.y, rect.height, rect.width, rect.color
                let point = new Point(Number(rect.x), Number(rect.y), Number(rect.width), Number(rect.height), rect.color)
                var pointStr = JSON.stringify(point)
                console.log("Points just tracked " + pointStr)
                points.push(point)
              });

            }
          });

          tracking.track('#img', tracker);
        });

        console.log("Points array " + JSON.stringify(points))
        if(points.length>10){
          sub.unsubscribe();
          this.cameraPreview.stopCamera();
          this.trackingFinished(points)
        }


      });

    },
    (err) => {
      console.log(err)
    });



    // var chain = $q.when();
    // for(var i = 0; i < 5; i++) {
    // 	chain = this.takePicture().then((rect) =>{
    //     if(rect){
    //       console.log("Returned Points " + rect)
    //     }
    //     else{
    //       console.log("No Rect " + rect)
    //     }
    //   });
    // }






      // console.log(this.loggedPoints)
      //
      // if(this.loggedPoints.length >= 9){
      //   this.navCtrl.push('DecisionPage')
      // }


  }

  trackingFinished(points){
    this.navCtrl.push(DecisionPage, {points: points})
  }

  takePicture(): any{
    const pictureOpts: CameraPreviewPictureOptions = {
      width: 350,
      height: 350,
      quality: 100
    }
    return(

      this.cameraPreview.takePicture(pictureOpts).then((imageData) => {

        this.picture = 'data:image/jpeg;base64,' + imageData;
        var img = this.picture;

        return 1;


        // console.log("Tracker is now " + tracker)
      }, (err) => {
        console.log(err);
      })

    );

  }


}
