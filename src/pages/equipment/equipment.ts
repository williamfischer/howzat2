import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SetUpPage } from '../set-up/set-up';

/**
 * Generated class for the EquipmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-equipment',
  templateUrl: 'equipment.html',
})
export class EquipmentPage {
  index: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.index = 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EquipmentPage');
  }

  goToSetUpPage(index){
    this.navCtrl.push(SetUpPage)
  }

}
