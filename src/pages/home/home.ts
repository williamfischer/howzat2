import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InstructPage } from '../instruct/instruct';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goToInstructionsPage(){
    this.navCtrl.push(InstructPage);
  }
}
