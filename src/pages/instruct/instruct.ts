import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EquipmentPage } from '../equipment/equipment';

/**
 * Generated class for the InstructPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-instruct',
  templateUrl: 'instruct.html',
})
export class InstructPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InstructPage');
  }

  goToChooseEquipmentPage(){
    this.navCtrl.push(EquipmentPage)
  }

}
