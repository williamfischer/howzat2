import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InstructPage } from './instruct';

@NgModule({
  declarations: [
    InstructPage,
  ],
  imports: [
    IonicPageModule.forChild(InstructPage),
  ],
})
export class InstructPageModule {}
