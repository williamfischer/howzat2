export class Point{
  x: number
  y: number
  width: number
  height: number
  color: string

  constructor(x: number, y: number, width: number, height: number, color: string){
    console.log("Point Created")
    this.x = x
    this.y = y
    this.width = width
    this.height = height
    this.color = color
  }

  calculateSomething(){
    console.log("Calc something with this object")
  }

  printPoint(){
    console.log("X: " + this.x + " Y: " + this.y + " Width: " + this.width + " Height: " + this.height + " Color: " + this.color);
  }
}
