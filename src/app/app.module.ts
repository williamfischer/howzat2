import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { DecisionPage } from '../pages/decision/decision';
import { EquipmentPage } from '../pages/equipment/equipment';
import { InstructPage } from '../pages/instruct/instruct';
import { LbwPage } from '../pages/lbw/lbw';
import { HomePage } from '../pages/home/home';
import { SetUpPage } from '../pages/set-up/set-up';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Camera } from '@ionic-native/camera';

import { CameraPreview } from '@ionic-native/camera-preview';

@NgModule({
  declarations: [
    MyApp,
    DecisionPage,
    EquipmentPage,
    InstructPage,
    LbwPage,
    SetUpPage,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DecisionPage,
    EquipmentPage,
    InstructPage,
    LbwPage,
    SetUpPage,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    CameraPreview,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})

export class AppModule {}
